
// анимация + на аккордионе
$(".accordion-button").on("click", function () {
    if ($(this).hasClass('open')) {
        $(this).removeClass('open');
    } else {
        $(".accordion-button.open").removeClass('open');
        $(this).addClass('open');
    }
});

// открытый аккордион
let accordionItems = $(".accordion-item");
accordionItems.each(function() {
  let collapseElement = $(this).find(".accordion-collapse");
  let buttonElement = $(this).find(".accordion-button");
  if (collapseElement.hasClass("show")) {
    buttonElement.addClass("open");
  } else {
    buttonElement.removeClass("open");
  }
});


// анимация стрелки

$(window).on('scroll',function() {
    let arrowBlock = $('.arrow-block');
    let careerFooterArrow = $('.career-footer__arrow');
    
    if (careerFooterArrow.is(':visible')) {
        setTimeout(function() {
          arrowBlock.css('left', '0');
        }, 1000); 
      }
  });

// анимация руки

  $(function() {
    setTimeout(function() {
      $('.career-header__advantages .advantages-item').addClass('advantages-item-hand');
    }, 1000);
  });